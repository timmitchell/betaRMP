function [data,counts] = getCumulativeCounts(data,bin_edges)
%   getCumulativeCounts:
%       Get the cumulative counts of data occurring in the bins specified
%       by bin_edges.  The values in bin_edges must be an row or column of
%       real values in strictly increasing order (no duplicates are
%       allowed).
%
%       bin_j:      Left-hand side closed, right-hand side open
%
%           [ bin_edges(j) bin_edges(j+1) )      j=1,...,numel(bin_edges)-1
%
%       bin_last:   A single point "bin"
%
%           [ bin_edges(end) bin_edges(end) ]
%
%       This is the convention that histc uses.  However, newer versions of
%       Matlab recommend that histcounts be used instead of histc.
%       However, histcounts uses a different binning convention, namely
%       that the last bin of histcounts is:
%
%           [ bin_edges(end-1) bin_edges(end) ]
%
%       On versions of Matlab where histcounts is available, this code will
%       use histcounts but the binning convention will be that of histc,
%       described above.
%
%
%   For comments/bug reports, please visit the betaRMP GitLab webpage:
%   https://gitlab.com/timmitchell/betaRMP
%
%   getCumulativeCounts.m introduced in betaRMP Version 1.0.
%
% =========================================================================
% |  getCumulativeCounts.m                                                |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is originally from URTM.                                   |
% |                                                                       |
% |  URTM is free software: you can redistribute it and/or modify         |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  URTM is distributed in the hope that it will be useful,              |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================
%
% =========================================================================
% |  betaRMP                                                              |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of betaRMP.                                        |
% |                                                                       |
% |  betaRMP is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of the   |
% |  License, or (at your option) any later version.                      |
% |                                                                       |
% |  betaRMP is distributed in the hope that it will be useful, but       |
% |  WITHOUT ANY WARRANTY; without even the implied warranty of           |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================
    
    persistent use_histc;
    
    assert( ~any(isnan(bin_edges)), 'bin_edges must not contain NaNs.');
    assert( issorted(bin_edges) &&                          ...
            numel(bin_edges) == numel(unique(bin_edges)),   ...
            'bin_edges must be an increasing order.'        );
     
    if isempty(use_histc)
        % histcounts was introduced in Matlab in R2014b
        % It is not currently available in Octave
        use_histc = isOctave() || ~atLeastVersion('2014b');
    end

    if use_histc
        counts          = histc(data,bin_edges);
        counts          = cumsum(counts);
    else
        % We just to replicate the last entry in bin_edges so that the last
        % bin is a single point bin and the penultimate bin then is open,
        % and thus excludes this last bin edge.
        bin_edges = [bin_edges bin_edges(end)];
        counts  = histcounts(data,bin_edges,'Normalization','cumcount');
    end

    data        = unique(data);
end
