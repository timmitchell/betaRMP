function retval = isOctave()
%   isOctave:
%       Returns true if the environment is Octave.
%
%
%   The code in this file is taken directly from the Octave website, which 
%   was published online without any license information.  Note that the 
%   AGPL license of betaRMP, and its subroutines, does not apply to the 
%   code in this single file.
%
% 
%   For comments/bug reports, please visit the betaRMP GitLab webpage:
%   https://gitlab.com/timmitchell/betaRMP
%
%   isOctave.m introduced in betaRMP Version 1.0.
%
% =========================================================================
% |  betaRMP                                                              |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of betaRMP.                                        |
% |                                                                       |
% |  betaRMP is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of the   |
% |  License, or (at your option) any later version.                      |
% |                                                                       |
% |  betaRMP is distributed in the hope that it will be useful, but       |
% |  WITHOUT ANY WARRANTY; without even the implied warranty of           |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    persistent cacheval;  % speeds up repeated calls

    if isempty (cacheval)
        cacheval = (exist ('OCTAVE_VERSION', 'builtin') > 0);
    end

    retval = cacheval;
end