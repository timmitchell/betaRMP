# Changelog

betaRMP

## Version 1.6 --- 2024-11-27

Description: bug fix

### Fixed
- Work around for the MATLAB issue, where MATLAB says error says takes 
  sprintf-like arguments but actually isn't fully compatible! 
  See the new comment in lines 420-425 of betaRMP.m.


## Version 1.5 --- 2020-02-04

Description: bug fix

### Fixed
- Fixed dimension mismatch error if cost field is not provided.  
  Thanks to Paul Schwerdtner for noticing this and seeing the fix.


## Version 1.4 --- 2019-08-19

Description: bug fix

### Fixed
- Correctly plots inf-RMP curves for methods that either don't have cost
  provided or have per-problem costs that were exactly zero.


## Version 1.3 --- 2018-11-07

Description: bug fix and documentation typos corrected

### Fixed
- Correctly plots a flat zero RMP curve for methods with no acceptable answers.
  Previously an error was thrown.

### Changed
- Moved CHANGELOG to CHANGELOG.md and changed to markdown format

### Maintenance
- Fixed minor typos in documentation and comments


## Version 1.2 --- 2017-08-16

Description: some more minor bug fixes

### Fixed
- opts.l1_total_violation default value should be false, not true
- Fixed bug where opts.budget_type == 1 was not allowed
- Fixed bug in calculating L1 total violation values
- Fixed bug in nan detection in .ci and .ce problem data


## Version 1.1 --- 2017-08-15

Description: improved documentation and errors, very minor bug fixes

### Fixed
- If a target value is (exactly) zero, an error is thrown, with some guidance
  given in the error message
- Fixed legend changing order when opts.plot_order is used (seemed to be a
  problem on newer Matlab releases)
- Fixed issue when computed relative differences are infs
- Fixed version/program name meta in comments

### Maintenance
- better documentation regarding the issue if a target value is exactly zero,
  since the relative difference to zero is not mathematically defined
- removed version numbers from all .m metadata, except betaRMP.m
- improved README
- added CHANGELOG


## Version 1.0 --- 2017-01-09

Description: initial public release
