function [h,scaled_budgets,targets] = betaRMP(method_data,opts)
%   betaRMP:
%       betaRMP is a routine for creating Relative Minimization Profiles,
%       or more specifically, beta-RMPs, proposed in [1].  beta-RMPs are
%       benchmarking visualization tools for simultaneously evaluating the
%       relative performance of multiple algorithms with respect to:
%
%       - objective quality (e.g. the amount of minimization achieved)
%       - feasibility       (constraints satisfied to tolerances)
%       - speed of progress (e.g. in terms of wall-clock time).
%
%       beta-RMPs highlight the trade-offs between these measures when
%       comparing algorithm performance.  Like performance profiles and
%       data profiles, beta-RMPs are generally intended for comparing
%       methods for optimization, but beta-RMPs can be used in other
%       contexts as well, such as assessing numerical accuracy of solvers
%       under different computational budget limits.
%
%       For a solver X, a point (a,b) on its RMP curve indicates that
%       solver X found feasible points (to tolerances) to b percent of the
%       test problems, under the condition that the best (lowest) objective
%       values observed at these feasible points were also either equal to
%       the best per-problem target values attainable on the (nearly)
%       feasible sets, or were at worst, only higher by at most a relative
%       difference of 10^a.
%
%       For a beta-RMP curve, and beta = infinity, a solver is judged
%       without any imposed per-problem computational budget.  For finite
%       values of beta, each solver is judged by the best solution it could
%       compute within a given per-problem computational budget, defined by
%       the specific value of beta and fixed per-problem budgets either
%       given explicitly by the user or extracted automatically from the
%       data.  The per-problem target values can also either be explicitly
%       given by the user or automatically extracted from the data.
%
%       betaRMP's convention is that the objective is minimization.  Thus
%       lower objective values are considered better solutions, provided
%       they are feasible.  For maximization problems, the user should
%       preprocess the corresponding objective data and target values
%       accordingly.  If the user wishes to assess the numerical accuracy
%       of various methods, the user must supply their own target values
%       explicitly via opts.target_values and set opts.target_minimums to
%       false.
%
%   IMPORTANT NOTE:
%       Target values, whether explicitly provided by the user or whether
%       extracted from the provided data, cannot be exactly zero.  The
%       issue is that relative differences to (exactly) zero are
%       mathematically undefined.  If this happens, betaRMP will throw an
%       error.  To work around this issue, one can consider adding a small
%       fixed shift to each of the objective histories for the offending
%       problem(s). The shift should be done to the objective histories for
%       all methods for the particular offending problems.  One should also
%       try several different shift values, as the shift values change the
%       computed relative differences and could thus lead to different RMP
%       curves.
%
%   USAGE:
%       [h,scaled_budgets,targets] = betaRMP(method_data)
%       [h,scaled_budgets,targets] = betaRMP(method_data,opts)
%
%   INPUT:
%       method_data     a cell array of length s, where each entry contains
%                       a struct of data for each of the s solvers in the
%                       comparison.  The struct for each solver has the
%                       following subfields:
%
%       .name           optional: give the solver a name to be used in the
%                       plots.  If no name is given, a default name will
%                       automatically be supplied.
%
%       .problem_data   required: a cell array of length n, where each
%                       entry contains a struct of data for each of the n
%                       problems in the comparison.  The struct for each
%                       problem has the following subfields:
%
%           .obj        row vector of length m containing the history of
%                       the objective values for this particular solver,
%                       from its first iterate to last (the mth iterate).
%
%           .ci         p x m matrix where each column contains the values
%                       of the p inequality constraint functions at each
%                       iterate in the solver's history for this problem.
%                       Positive entries indicate constraint violations.
%
%           .ce         q x m matrix where each column contains the values
%                       of the q equality constraint functions at each
%                       iterate in the solver's history for this problem.
%                       Nonzero entries indicate constraint violations.
%
%           For constrained problems, either .ci and/or .ce must be
%           provided.  However, one can instead populate .ci with violation
%           values instead of the values of the constraint functions, if
%           necessary, though then, one should make sure that the violation
%           measures are consistent amongst the solvers being compared.
%
%           For unconstrained problems, .ci and .ce do not need to be
%           specified, provided the user sets opts.constrained to false.
%           Note that this is a global setting for all problems.  To mix
%           unconstrained and constrained problems, either .ci or .ce will
%           need to be specified as zero row vectors of appropriate length
%           for each unconstrained problem, e.g. .ci = zeros(1,m).
%
%           .cost       row vector of length m containing the cumulative
%                       costs (generally wall-clock time) for obtaining
%                       each iterate.  All entries must be nonnegative and
%                       in increasing order.  For example, if it takes 1
%                       second to obtain the first iterate, and two seconds
%                       to obtain the second iterate, the first two entries
%                       of .cost will be 1 and 3.  If this specific
%                       per-iterate data is unobtainable, it is often okay
%                       to use the average cost per iterate, i.e.
%
%                           avg_cost = total_cost / number_of_iters;
%
%                       to create a surrogate of this cumulative cost data:
%
%                           .cost = avg_cost:avg_cost:total_cost;
%
%           If no finite values of betas are requested, the .cost data can
%           be omitted, as only a beta-RMP for beta = infinity will be
%           produced, which assumes an infinite computational budget for
%           each problem.
%
%       opts            optional struct of user-settable parameters:
%
%       .betas              [ length k row vector | {[inf]} ]
%           Row vector of unique positive values specifying the list of
%           beta-RMP plots to generate.
%
%       .constrained        [ logical | {true} ]
%           Specifies whether the problems are constrained or
%           unconstrained.  If they are constrained, either .ci and/or .ce
%           subfields must be provided for every solver problem pair in the
%           .problem_data cell arrays supplied for each solver.
%
%       .l1_total_violation [ logical | {false} ]
%           By default, the constraint violation is measured as the
%           maximal constraint violation observed over all the constraints
%           (i.e. the infinity-norm).  Alternatively, the constraint
%           violation can be measured using the 1-norm by setting
%           opts.l1_total_violation to true; in this case, constraint
%           violation is measured as the sum of all constrained violations.
%           Note that the inequality constraints violation and equality
%           constraint violation are measured separately, as it is may be
%           desirable to use different tolerances.  For example, it may
%           sometimes be reasonable to require that the inequality
%           constraints be satisfied with zero tolerance for violation but
%           the tolerance for equality constraint violation must generally
%           be positive, since it is not realistic to satisfy equality
%           constraints exactly.
%
%       .viol_ineq_tol      [ nonnegative finite real | {1e-6} ]
%           An iterate satisfies the inequality constraint functions if the
%           inequality constraint violation is less than or equal to
%           viol_ineq_tol.  Note that this can be measured in two different
%           ways.  See opts.l1_total_violation for more details.
%
%       .viol_eq_tol        [ nonnegative finite real | {1e-6} ]
%           An iterate satisfies the equality constraint functions if the
%           equality constraint violation is less than or equal to
%           viol_eq_tol.  Note that this can be measured in two different
%           ways.  See opts.l1_total_violation for more details.
%
%       .budget_type        [ one entry of either [amfs] or [1:s] | {'f'} ]
%           Specifies the type of per-problem budget to use.  For each
%           problem, each solver is defined by its performance under a
%           specific computational budget for that problem, scaled by the
%           given value of beta for the beta-RMP plot.  The computational
%           budgets can either be provided explicitly or they can be
%           generated automatically for the data.  These values can be
%           generated in various ways.  For each problem, each solver is
%           only allowed:
%
%               'a'     the Average cost of the s solvers to solve this
%                       particular problem
%
%               'm'     the Median cost of the s solvers to solve this
%                       particular problem
%
%               'f'     the cost of the Fastest solver on this particular
%                       problem
%
%               's'     the cost of the Slowest solver on this particular
%                       problem
%
%               j       the cost of the jth solver (in the order specified
%                       by method_data) on this particular problem.  j must
%                       be a positive integer between 1 and s.
%
%       .budget_values      [ 1 x n vector of values in [0,inf) | {[]} ]
%           The user may optionally specify their own per-problem budgets
%           if desired here.
%
%       .target_values      [ 1 x n vector of finite values | {[]} ]
%           The user may optionally specify their own per-problem target
%           values.  Note that exactly zero is not currently supported as a
%           target value.  Also, if opts.target_minimums is set to true
%           (its default), user-supplied target values will be overridden
%           if better (lower) values are observed in the data.
%
%       .target_minimums    [ logical | {true} ]
%           By default, betaRMP assumes that the test problems are
%           minimization problems and that the best solution to a problem
%           is one which attains the lowest objective value at point in the
%           (nearly) feasible set (as determined by the two violation
%           tolerances).  However, beta-RMPs can also assess numerical
%           accuracy of solvers, by measuring deviations above and below
%           user-supplied target values.  To do so, target values must be
%           explicitly provided via opts.target_values and one must set
%           opts.target_minimums to false.
%
%       .target_scaling     [ logical | {true} ]
%           If beta-RMPs are being generated for finite values of beta, the
%           target values can either be fixed or scaled.  For the former,
%           each beta-RMP figure shares the same target value set,
%           defined by taking the best (lowest) target values encountered
%           on the (nearly) feasible sets (obtained from observing all the
%           iterates of all s solvers).  However, when opts.target_scaling
%           is enabled, then each beta-RMP figure uses a more limited
%           target value set, where the data-derived target values must be
%           (nearly) feasible points which were computed by the s solvers
%           within their respective scaled per-problem budgets defined by
%           beta.  If opts.target_values is additionally set, the target
%           value set will be defined by taking the better of the
%           user-supplied and data-obtained values for each problem.  Note
%           that opts.target_scaling has no effect when using beta-RMPs to
%           assess numerical accuracy, that is, when target values have
%           been explicitly provided (via opts.target_values) and
%           opts.target_minimums has been set to false.
%
%       .plot_range         [ two increasing integers >= -15 | {[-15 1]} ]
%           Each beta-RMP plots a relative difference measure, in log10
%           scale on the x-axis.  On the left, -16 is always included (the
%           limits of machine precision), while on the right, infinity is
%           always included (where only attaining feasibility matters and
%           the relative difference measure is ignored).  The user can then
%           set the intermediate range to display.  The plots will compress
%           the data between -16 and the opts.plot_range(1) and between
%           opts.plot_range(1) and infinity as necessary.
%
%       .plot_order         [ permutation of [1:s] | {[1:k]} ]
%           By default, each solver's RMP curve is plotted in the order
%           given by method_data.  However, if this drawing is order
%           inconvenient, e.g. a method of interest has its RMP curve
%           obscured by another RMP curve being drawn over it, the draw
%           order of the RMP curves can be specified with this option.
%           Note that this does not change the ordering of the methods or
%           the legend entries, just the order that the curves are "drawn".
%
%       .legend_opts        [ struct of legend() options | {struct()} ]
%           By default, the legend is placed in the NorthWest corner of
%           each beta-RMP figure.  This can be changed by creating
%           subfields corresponding to option names of Matlab's legend
%           function.  For example, if there are four values of beta, one
%           can specify the legend locations for each of the four beta-RMP
%           figures by setting:
%
%               .legend_opts.Location = {'North','East','South','West'};
%
%           Other legend options can be set in a similar manner.  The
%           values should either be row vectors or cell arrays.  Note that
%           values in these option subfields will either be repeated and/or
%           cropped to obtain the corresponding numbers of specifications
%           for the number beta values requested.  Thus, the legend
%           location can be set globally for all beta-RMPs by setting:
%
%               .legend_opts.Location = {'North'};
%
%           while an alternating location pattern can be done by setting
%
%               .legend_opts.Location = {'North','South'};
%
%       .line_opts          [ struct of plot() options | {struct()} ]
%           By default, each RMP curve is plotted with a LineWidth of 2,
%           using the standard lines colormap and repetition of LineStyle
%           types {'-','--','-.',':'}.  For example, if six solvers are
%           being compared, the sixth RMP curve will be, by default,
%           plotted with a '--' LineStyle and a light blue Color.  If the
%           user wishes to set their own line plots styles, that may be
%           done by creating fields corresponding to option names of
%           Matlab's stairs function.  For example, one can specify that
%           red, green, and blue are used, with the first three solvers
%           using solid lines, and the last three solvers using dashed
%           lines, by specifying
%
%               .line_opts.Color        = {[1 0 0], [0 0.5 0], [0 0 1]};
%               .line_opts.LineStyle    = {'-','-','-','--','--','--'};
%
%           This will cause the 1st and 4th solvers to be plotted in red,
%           respecitvely with solid and dashed lines, the 2nd and 5th in
%           green, etc.  Note that values in these option subfields will
%           either be repeated and/or cropped to obtain the corresponding
%           numbers of specifications for the s solvers.  Note that the
%           convenience "LineSpec" third argument to plot()/stairs(),
%           which is not preceded by a name argument, is NOT supported.
%
%       NOTE:   Options specified in opts.legend_opts or opts.line_opts
%               must observe proper capitalization.  For example,
%               'LineStyle' is proper while 'linestyle' is NOT supported.
%
%   OUTPUT:
%       h               length k vector of figure handles for each of the
%                       beta-RMPs created.  The beta-RMP figures are
%                       created in order of increasing value of beta.
%
%       scaled_budgets  a k x n matrix of scaled per-problem budgets.  The
%                       jth row defines the scaled per-problem budgets used
%                       for the jth beta-RMP.
%
%       target_values   a k x n matrix of target values.  The jth row
%                       defines the per-problem target values for the jth
%                       beta-RMP.  Note if the user has provided explicit
%                       target values, they can compare this matrix data
%                       to their specified tagets to see which budget
%                       problem pairs were overridden to use target values
%                       extracted from the data (assuming that parameter
%                       opts.target_minimums was true, its default value).
%
%
%   If you publish work that either refers to or makes use of RMPs, please
%   cite the following paper:
%
%   [1] Frank E. Curtis, Tim Mitchell, and Michael L. Overton
%       A BFGS-SQP method for nonsmooth, nonconvex, constrained
%       optimization and its evaluation using relative minimization
%       profiles, Optimization Methods and Software, 32(1):148-181, 2017.
%       Available at https://dx.doi.org/10.1080/10556788.2016.1208749
%
%   For comments/bug reports, please visit the betaRMP GitLab webpage:
%   https://gitlab.com/timmitchell/betaRMP
%
%   betaRMP Version 1.5, 2016-2020, see AGPL license info below.
%   betaRMP.m introduced in betaRMP Version 1.0.
%
% =========================================================================
% |  betaRMP                                                              |
% |  Copyright (C) 2016 Tim Mitchell                                      |
% |                                                                       |
% |  This file is part of betaRMP.                                        |
% |                                                                       |
% |  betaRMP is free software: you can redistribute it and/or modify      |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of the   |
% |  License, or (at your option) any later version.                      |
% |                                                                       |
% |  betaRMP is distributed in the hope that it will be useful, but       |
% |  WITHOUT ANY WARRANTY; without even the implied warranty of           |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/>.                                      |
% =========================================================================

    if nargin < 2
        opts    = struct();
    end
    opts        = processRMPOpts(opts);
    betas       = opts.betas;
    n_betas     = length(betas);

    % Ensure all methods have a name (assign a default name if necessary)
    % and ensure all methods have the same number of problems.
    [method_data,n_solvers,n_probs] = checkMethodData(method_data);

    % process the plotting options and make plotting functions with the
    % user's arguments embedded
    opts = processPlotOpts(opts,n_betas,n_solvers);

    % Ensure each problem's given data is correct and then reformat and
    % convert it all so that each problem will contain .obj, .feasible, and
    % .cost subfields, all vectors of the same length.
    data    = cell(n_solvers,n_probs);
    names   = cell(1,n_solvers);
    for j = 1:length(method_data)
        [data(j,:),names{j}] = getMethodData(method_data,j,opts);
    end

    % Get the budgets, either explicitly provided by the user or generated
    % via the data itself
    [budgets,budget_str]    = getBudgetValues(data,names,n_probs,opts);
    scaled_budgets          = betas'*budgets;
    
    % If one of the betas is inf, any zero budgets will result in nans (inf
    % times zero), but these scaled budgets should really be inf.
    idx_inf                 = isinf(betas);
    idx_zero                = budgets == 0;
    scaled_budgets(idx_inf,idx_zero) = inf;

    get_title_fn            = @(beta) makeTitle(beta,budget_str);

    % Get the target values.  A user target value will only replace a
    % data-derived target value if the user target values is better than
    % the data-derived value (for each given scaled budget).
    targets = getTargetValues(data,scaled_budgets,n_betas,n_probs,opts);

    % We can now process the data to make beta-RMPs
    h = zeros(1,n_betas);
    for j = 1:n_betas
        assertNoZeros(targets(j,:),betas(j));
        h(j) = makeRMPPlot( data, scaled_budgets(j,:), targets(j,:),    ...
                            opts, names, get_title_fn(betas(j))         );
    end
end

function assertNoZeros(targets,beta)
    tz  = targets == 0;
    if any(tz)
        prob_num        = find(tz);
        prob_num_fmt    = repmat('%d ',1,length(prob_num));
        str = sprintf(                                                  ...
            [ 'betaRMP: cannot make %g-RMP plot.\n'                     ...
              'Target values cannot be exactly zero.\n'                 ...
              'Occurs on problems: ' prob_num_fmt '\n',                 ...
              'See the IMPORTANT NOTE section of ''help betaRMP'' '     ...
              'for more information.\n'],                               ...
              beta,   prob_num                                          );
        % Although MATLAB advises error takes sprintf like arguments, 
        % sprintf can take an array input argument to populate multiple 
        % format spec tokens, which is used above to print out the problem
        % numbers in a concise way.  The error function on R2024a does not
        % support this feature and so we instead built the string
        % separately via sprintf and then pass it to error.
        error(str);
    end
end

function opts = processRMPOpts(opts)

    assertFn        = getAssertFn('invalidInputData','opts');
    assertFn(isstruct(opts),'must be a struct.');

    get_assert_fn   = @(option) getAssertFn('invalidOption',['.' option]);

%     default_opts    = struct(   'constrained',          true,       ...
%                                 'l1_total_violation',   false,      ...
%                                 'viol_ineq_tol',        1e-6,       ...
%                                 'viol_eq_tol',          1e-6,       ...
%                                 'betas',                inf         );

    if ~isfield(opts,'constrained')
        opts.constrained        = true;
    else
        assertFn    = get_assert_fn('constrained');
        assertFn(islogical(opts.constrained),'must be a logical.');
    end

    if ~isfield(opts,'l1_total_violation')
        opts.l1_total_violation    = false;
    else
        assertFn    = get_assert_fn('l1_total_violation');
        assertFn(islogical(opts.l1_total_violation),'must be a logical.');
    end

    if ~isfield(opts,'viol_ineq_tol')
        opts.viol_ineq_tol      = 1e-6;
    else
        assertFn    = get_assert_fn('viol_ineq_tol');
        tol         = opts.viol_ineq_tol;
        assertFn(   length(tol) == 1 && isfinite(tol) && tol >= 0 &&    ...
                    isreal(tol),                                        ...
                    'must be a nonnegative finite real value.'          );
    end

    if ~isfield(opts,'viol_eq_tol')
        opts.viol_eq_tol        = 1e-6;
    else
        assertFn    = get_assert_fn('viol_eq_tol');
        tol         = opts.viol_eq_tol;
        assertFn(   length(tol) == 1 && isfinite(tol) && tol >= 0 &&    ...
                    isreal(tol),                                        ...
                    'must be a nonnegative finite real value.'          );
    end

    if ~isfield(opts,'betas')
        opts.betas              = inf;
    else
        assertFn    = get_assert_fn('betas');
        betas       = opts.betas;
        uni_betas   = unique(betas(:))';
        assertFn(   isVector(betas) && isreal(betas) &&                 ...
                    size(betas,1) == 1  && ~any(betas <= 0) &&          ...
                    numel(betas) == numel(uni_betas),                ...
                    [   'must be a row vector of positive unique real ' ...
                        'values.'   ]                                   );
        opts.betas  = uni_betas;
    end
end

function opts = processPlotOpts(opts,n_betas,n_solvers)

    get_assert_fn   = @(option) getAssertFn('invalidOption',['.' option]);
    min_val         = floor(log10(eps));
    opts.min_val    = min_val;

%     default_opts    = struct(   'plot_range',       [min_val+1,1],  ...
%                                 'plot_order',       1:n_solvers,    ...
%                                 'line_opts',        struct(),       ...
%                                 'legend_opts',      struct()        );

    if ~isfield(opts,'plot_range')
        opts.plot_range = [min_val+1,1];
    else
        r           = opts.plot_range;
        assertFn    = get_assert_fn('plot_range');
        assertFn(   isnumeric(r) && length(r) == 2 && ~any(mod(r,1)) && ...
                    r(1) > min_val && r(2) > r(1) && r(2) < inf,        ...
                    [   'must be an pair of finite integers, in '       ...
                        'increasing order and greater than the size of '...
                        'floor(log10(eps)).']                           );
    end

    if ~isfield(opts,'plot_order')
        opts.plot_order = 1:n_solvers;
    else
        o           = opts.plot_order;
        assertFn    = get_assert_fn('plot_order');
        assertFn(   isnumeric(o) && length(o) == n_solvers &&           ...
                    ~any(sort(o) - (1:n_solvers)),                      ...
                    'must be a permutation of integers [1,...,%d].',    ...
                    n_solvers                                           );
    end

    % Set a few defaults for plotted lines
    if ~isfield(opts,'line_opts')
        opts.line_opts = struct();
    else
        assertFn = getAssertFn('invalidOption','.line_opts');
        assertFn(isstruct(opts.line_opts),'must be a struct.')
    end
    line_opts = opts.line_opts;
    if ~isfield(line_opts,'LineStyle')
        line_opts.LineStyle  = {'-','--','-.',':'};
    end
    if ~isfield(line_opts,'Color')
        line_opts.Color     = num2cell(lines(7),2)';
    end
    if ~isfield(opts.line_opts,'LineWidth')
        line_opts.LineWidth = 2;
    end
    opt_names = fieldnames(line_opts);
    plot_args = cell(n_solvers+1,length(opt_names));
    for j = 1:length(opt_names)
        name    = opt_names{j};
        options = replicateToLength(    line_opts.(name),   ...
                                        n_solvers,          ...
                                        ['line_opts.' name] );
        if ~iscell(options)
            options = num2cell(options);
        end
        plot_args{1,j}      = name;
        plot_args(2:end,j)  = options(:)';
    end

    function args = getPlotOpts(k)
        args = plot_args([1 k+1],:);
        args = args(:);
    end

    % Set a default position for the legend
    if ~isfield(opts,'legend_opts')
        opts.legend_opts = struct();
    else
        assertFn = getAssertFn('invalidOption','.legend_opts');
        assertFn(isstruct(opts.legend_opts),'must be a struct.')
    end
    legend_opts = opts.legend_opts;
    if ~isfield(legend_opts,'Location') || ~isfield(legend_opts,'Position')
        legend_opts.Location = {'NorthWest'};
    end
    opt_names   = fieldnames(legend_opts);
    legend_args = cell(n_betas+1,length(opt_names));
    for j = 1:length(opt_names)
        name    = opt_names{j};
        options = replicateToLength(    legend_opts.(name),     ...
                                        n_betas,                ...
                                        ['legend_opts.' name]);
        if ~iscell(options)
            options = num2cell(options);
        end
        legend_args{1,j}        = name;
        legend_args(2:end,j)    = options(:)';
    end

    function args = getLegendOpts(k)
        args = legend_args([1 k+1],:);
        args = args(:);
    end

    opts.get_plot_opts   = @getPlotOpts;
    opts.get_legend_opts = @getLegendOpts;
end

function t = makeTitle(beta,budget_str)
    if isinf(beta)
        num_str = '\infty';
    elseif mod(beta,1) == 0
        num_str = sprintf('%d',beta);
    else
        num_str = sprintf('%.3g',beta);
    end
    t = {['\beta-RMP (\beta = ' num_str ')'],budget_str};
end

function c = replicateToLength(c,n,option_name)

    assertFn    = getAssertFn('invalidOption',['.' option_name]);
    [rows,cols] = size(c);
    assertFn(rows == 1 && cols > 0, 'must be a row vector.');

    % make array c have length n, by repeating it as many times as
    % necessary and then cropping it, if needed
    r = ceil(n / length(c));
    c = repmat(c,1,r);
    c = c(1:n);
end

function h = makeRMPPlot(data,budgets,targets,opts,names,title_str)
    h           = figure();

    % plot all the RMP curves, in their given order
    rmp_ui      = plotRMPCurves(data,budgets,targets,opts);

    % uistack now affects the legend order on newer Matlab versions
    % So we'll plot first, reorder the lines, and then make the legend
    % using the specific handles to each plotted curve (which are still in
    % the original order

    % Change the order that the curves are drawn to the plot.
    for k = 1:size(data,1)
        uistack(rmp_ui(opts.plot_order(k)),'top');
    end

    legend_args = opts.get_legend_opts(1);
    % Specifically pass the handles to legend so that the legend order
    % matches whatever order was provided by the user as input.
    legend(rmp_ui,names,legend_args{:});

    setAxes(opts.plot_range,opts.min_val);
    title(title_str);
end

function rmp_ui = plotRMPCurves(data,budgets,targets,opts)
    [n_solvers,n_probs] = size(data);

    % draw all the individual RMP curves in their given order
    rmp_ui  = zeros(1,n_solvers);
    for j = 1:n_solvers
        [rel_diffs,counts]  = makeRMPCurve(data(j,:),budgets,targets);
        percent_solved      = counts/n_probs;
        plot_args           = opts.get_plot_opts(j);
        rmp_ui(j)           = plotRMPCurve(     rel_diffs,      ...
                                                percent_solved, ...
                                                opts.plot_range,...
                                                opts.min_val,   ...
                                                plot_args       );
        hold on
    end
end

function setAxes(range,min_val)
   % setup x-axis
    x_lims          = [range(1) - 2, range(2) + 2];
    x_ticks         = range(1)-1:range(2)+1;
    x_labels        = num2cell(x_ticks);
    x_labels{1}     = min_val;
    x_labels{end}   = '\infty';
    ax              = gca;
    xlim(x_lims);
    set(ax,'xtick',x_ticks);
    set(ax,'xticklabel',x_labels);
    xlabel('within 10^x relative difference of target values');

    % setup y-axis
    ylim([0 1.025]);
    set(gca,'ytick',0:0.1:1);
    set(gca,'yticklabel',0:10:100);
    ylabel('% of feasible optimizers (to tolerances)');
    box(ax(1),'off');
    grid on
end

function h = plotRMPCurve(rel_diffs,counts,range,min_val,plot_args)
    % the RMP Curve is plotted on a log10 x-axis; these are exponents
    min_log10       = range(1);
    max_log10       = range(2);
    eps_log10       = min_val;  % should be -16 for 64 bit hardware

    % total all the counts of the relative differences below machine
    % precision and flatten them into the leftmost x-axis 10^-16 tick mark
    min_indx        = rel_diffs <= 10^eps_log10;
    if any(min_indx)
        min_counts  = counts(min_indx);
        min_count   = min_counts(end);
    else
        min_count   = 0;
    end
    counts          = [min_count counts(~min_indx)];
    rel_diffs       = [eps_log10 log10(rel_diffs(~min_indx))];

    % compress the relative differences between machine precison and the
    % minimum of the range being requested
    compressed      = min_log10 - eps_log10;
    if compressed > 1
        min_indx    = rel_diffs < min_log10;
        % rescale these so that they are in [min_log10-1,min_log10)
        % first get them to be in [0,1)
        min_rd              = (rel_diffs(min_indx) - eps_log10)/compressed;
        rel_diffs(min_indx) = min_rd + min_log10 - 1;
    end

    % get all the individual counts between the maximum being plotted and
    % infinity and rescale them so that they fit between the last two
    % x-axis tick marks
    max_indx        = rel_diffs > max_log10;
    % rescale these so that they are (max_log10,max_log10+1]
    if any(max_indx)
        max_rd      = rel_diffs(max_indx) - max_log10;
        max_rd      = (max_rd / max_rd(end)) + max_log10;
        max_rd      = [max_rd max_rd(end)];
    else
        max_rd      = max_log10 + 1;
    end
    rel_diffs       = rel_diffs(~max_indx);

    x_vals          = [rel_diffs max_rd max_log10+2];
    counts          = [counts counts(end) counts(end)];
    h               = stairs(x_vals,counts,plot_args{:});
end

function [rel_diffs,counts] = makeRMPCurve(data,budgets,targets)

    % extract the best feasible value within the budget for each problem
    budgets     = num2cell(budgets);
    best_vals   = cellfun(@getBestObjectiveWithinBudget,data,budgets);

    % only care about problems where at least one solver found a feasible
    % point and where the current solver found a feasible point
    indx        = ~isnan(best_vals) & ~isnan(targets);

    % If none were feasible, return 0 for both
    if ~any(indx)
        rel_diffs   = 0;
        counts      = 0;
        return
    end

    best_vals   = best_vals(indx);
    targets     = targets(indx);

    % Taking the absolute value of the numerator is not strictly necessary
    % since, by construction, the data-derived values can never be lower
    % than the target values.  However, if opts.target_minimize is true,
    % data-derived values are not used, since the user has specified that
    % the intention is to assess numerical accuracy of attaining explicitly
    % given target values.  Hence, we must include the absolute value.
    rel_diffs   = abs(best_vals - targets) ./ abs(targets);
    rel_diffs   = sort(rel_diffs);

    % get the frequencies of all the relative differences
    % and form a cumulative sum
    bin_ranges          = unique(rel_diffs);
    [rel_diffs,counts]  = getCumulativeCounts(rel_diffs,bin_ranges);
end

function [data,s,n] = checkMethodData(data)
    for j = 1:length(data)
        data{j} = setName(data{j},j);
    end
    sizes = cellfun(@checkProblemDataIsPresent,data);
    if any(sizes(1) - sizes)
        error(  'betaRMP:invalidInputData',                             ...
                [   'each method must supply the same number of '       ...
                    'problems in their .problem_data fields']           );
    end
    s = length(sizes);
    n = sizes(1);
end

function n = checkProblemDataIsPresent(data)
    assertFn = getAssertFn('invalidInputData',data.name);
    assertFn(   isfield(data,'problem_data'),                           ...
                '.problem_data field is missing.'                       );
    assertFn(   min(size(data.problem_data)) == 1,                      ...
                '.problem_data must be a one dimensional cell array.'   );
    n = length(data.problem_data);
end

function data = setName(data,number)
    % get method's name provided by user (or give it a default one)
    default_name = sprintf('Method %d',number);
    if isfield(data,'name')
        fprintf('%s is %s\n',default_name,data.name);
    else
        data.name = default_name;
        fprintf('%s has no .name field; using "%s" as its name.\n',     ...
                default_name,default_name                               );
    end
end

function [data,name] = getMethodData(all_data,method_indx,opts)

    method_data     = all_data{method_indx};
    data            = method_data.problem_data;
    name            = method_data.name;

    constrained     = opts.constrained;
    beta_finite     = any(opts.betas < inf);
    isFeasibleFn    = @(ci,ce) isFeasible(  ci, ce,                 ...
                                            opts.viol_ineq_tol,     ...
                                            opts.viol_eq_tol,       ...
                                            opts.l1_total_violation );

    % iterate over the problems for this method and convert/format their
    % data in obj, feasible, and cost fields, throwing errors if any
    % data is missing or invalid.
    for j = 1:length(data)

        assertFn    = getAssertFn(  'invalidInputData',                 ...
                                    sprintf('%s, Problem #%d',name,j)   );
        dj          = data{j};

        obj         = getObjectiveValues(dj);
        feas        = getFeasibles(dj);
        cost        = getCostValues(dj,length(obj)); % # of iters

        % only keep the feasible iterates except for the last, which will
        % kept regardless of whether it is feasible or not
        indx        = false(1,length(feas));
        indx(end)   = true;
        indx        = feas | indx;

        data{j}     = struct(   'obj',      obj(indx),      ...
                                'feasible', feas(indx),     ...
                                'cost',     cost(indx)      );
    end

    function obj = getObjectiveValues(data)
        % make sure objective values are included
        assertFn(   isfield(data,'obj'),                                ...
                    [   '.obj field is missing, a vector of the '       ...
                        'objective values at each iterate.']            );
        obj = data.obj;
        assertFn(   isVector(obj) && size(obj,1) == 1,                  ...
                    '.obj must be a row vector.'                        );
        assertFn(   allFinite(obj) && isreal(obj),                      ...
                    '.obj must only contain finite real-valued entries.');
    end

    function feasible = getFeasibles(data)
        assertFn(   ~constrained || any(isfield(data,{'ci','ce'})),     ...
                    [   'either .ci or ce field must be provided, '     ...
                        'respectively row vectors of the inequality '   ...
                        'or equality constraint values.']               );

        n   = length(data.obj);
        % get whatever constraint values are provided
        ci  = getConstraintValues(data,n,'ci');
        ce  = getConstraintValues(data,n,'ce');

        feasible = true(1,n);

        % calculate which iterates were infeasible
        for k = 1:n
            feasible(k) = isFeasibleFn(ci(:,k),ce(:,k));
        end
    end

    function c = getConstraintValues(data,n,fieldname)
        if isfield(data,fieldname)
            c = data.(fieldname);
            [rows,cols] = size(c);
            assertFn(   cols == n,                                      ...
                        [   'the number of columns of .%s field must '  ...
                            'match the length of .obj.'], fieldname     );
            assertFn(   rows > 0,                                       ...
                        [   'the number of rows of .%s field must be '  ...
                            'positive.'], fieldname                     );
            assertFn(   isreal(c) && ~any(isnan(c(:))),                 ...
                        'entries of .%s must all be real valued.',      ...
                        fieldname                                       );
        else
            c = zeros(1,n);
        end
    end

    function cost = getCostValues(data,n)
        has_cost = isfield(data,'cost');
        assertFn(~beta_finite || has_cost,'.cost field is missing.');
        if has_cost
            cost = data.cost;
            assertFn(   isVector(cost) && size(cost,2) == n,            ...
                        '.cost must have the same dimensions as .obj.'  );
            assertFn(   allFinite(cost) && ~any(cost < 0),              ...
                        [   '.cost must only contain finite '           ...
                            ' nonnegative real-valued entries.']        );
            assertFn(   ~any((cost(2:end) - cost(1:end-1)) < 0),        ...
                        [   'values in .cost must be monotonically '    ...
                            'increasing.']                              );
        else
            cost = zeros(1,n);
        end
    end
end

function [b,b_str] = getBudgetValues(data,names,n,opts)
    assertFn = getAssertFn('invalidOption','.budget_values');
    if isfield(opts,'budget_values')
        b = opts.budget_values;
        assertFn(   isVector(b) && length(b) == n,                      ...
                    [   'the number of budgets must equal the number '  ...
                        'of problems.']                                 );
        assertFn(   allFinite(b) && ~any(b <= 0),                       ...
                    'all entries must be finite positive real values.'  );
        b_str = 'user-specified';
    else
        [b,b_str] = getBudgetValuesFromType(data,names,opts);
    end
    b_str = ['Budgets: \beta \times (for each problem: ' b_str ')'];
end

function [budgets,budget_str] = getBudgetValuesFromType(data,names,opts)
    assertFn = getAssertFn('invalidOption','.budget_type');

    if ~isfield(opts,'budget_type')
        opts.budget_type = 'f';
    end

    % extract number of solvers and problems and final cost for each
    % (solver,problem) pair
    [s,n]   = size(data);
    costs   = zeros(s,n);
    for j = 1:s
        for k = 1:n
            costs(j,k) = data{j,k}.cost(end);
        end
    end

    type    = lower(opts.budget_type);
    assertFn(length(type) == 1,'option must be a single value.');

    if isnumeric(type)
        assertFn(   any( (1:s) == type ) ~= 0,                          ...
                    [   'value == %d does not match any of the method ' ...
                        'indices [1,...,%d].'],type,s                   );
        budgets = costs(type,:);
        budget_str = sprintf('cost of %s',names{type});
    elseif ischar(type)
        switch type
            case 'a'
                budgets     = mean(costs);
                budget_str  = 'average';
            case 'm'
                budgets     = median(costs);
                budget_str  = 'median';
            case 'f'
                budgets     = min(costs);
                budget_str  = 'minimum';
            case 's'
                budgets     = max(costs);
                budget_str  = 'maximum';
            otherwise
                optionError('.budget_type: unrecognized type.');
        end
        budget_str = sprintf(   '%s of the costs of the %d methods',    ...
                                budget_str,s);
    else
        optionError('.budget_type must be an int or char.');
    end
end

function t = getTargetValues(data,scaled_budgets,k,n,opts)

    if ~isfield(opts,'target_minimums')
        opts.target_minimums = true;
    end

    t_user  = getTargetValuesFromUser(opts,n);
    if ~opts.target_minimums && ~isempty(t_user)
        t   = t_user;
        t   = repmat(t,k,1);
        return
    end
    t       = extractAllTargetValuesFromData(data,scaled_budgets,opts);

    if ~isempty(t_user)
        user_values_updated = false;
        for j = 1:k
            % only use the user values if they are at least as good as the
            % data-derived target values
            indx        = t_user <= t(j,:);
            t(j,indx)   = t_user(indx);

            if any(~indx)  % at least one user was worse than data
                user_values_updated = true;
            end
        end
        if user_values_updated
            fprintf([   'betaRMP: at least some data-derived target '   ...
                        'values are better than user values. '          ...
                        'Replacing.\n']                                 );
        end
    end
end

function t = getTargetValuesFromUser(opts,n)
    assertFn = getAssertFn('invalidOption','.target_values');
    if isfield(opts,'target_values')
        t = opts.target_values;
        assertFn(   isVector(t) && length(t) == n,                      ...
                    [   'the number of target values does not match '   ...
                        'the number of problems.']                      );
        assertFn(   allFinite(t),                                       ...
                    'target values must be finite and real-valued.'     );
        assertFn(   ~any(t == 0),                                       ...
                [   'exactly zero target values are not currently '     ...
                    'supported.']                                       );
    else
        t = [];
    end
end

function t = extractAllTargetValuesFromData(data,scaled_budgets,opts)
    assertFn = getAssertFn('invalidOption','.target_scaling');

    % set a default behavior if no user option is given
    if ~isfield(opts,'target_scaling')
        opts.target_scaling = true;
    end

    scaling         = opts.target_scaling;
    assertFn(islogical(scaling),'must be a logical.');
    [n_budgets,n]   = size(scaled_budgets);

    if scaling
        t = zeros(n_budgets,n);
        for j = 1:n_budgets
            t(j,:) = getTargetValuesFromData(data,scaled_budgets(j,:),n);
        end
    else
        t = getTargetValuesFromData(data,inf(1,n),n);
        t = repmat(t,n_budgets,1);
    end
    assertFn(   any(t(:) ~= 0),                                         ...
                [   'exactly zero target values obtained from data; '   ...
                    'exactly zero target values are not currently '     ...
                    'supported.']                                       );
end

function t = getTargetValuesFromData(data,budgets,n)
    t = zeros(1,n);
    for j = 1:n
        get_best_fn = @(d) getBestObjectiveWithinBudget(d,budgets(j));
        t(j)        = min(cellfun(get_best_fn,data(:,j)));
    end
end

function f = getBestObjectiveWithinBudget(data,budget)
    obj     = data.obj;
    cost    = data.cost;
    feas    = data.feasible;

    indx    = cost <= budget & feas;
    f       = min(obj(indx));
    if isempty(f)
        f   = nan;
    end
end

function tf = isFeasible(ci,ce,vi_tol,ve_tol,l1_total_violation)
    tf = false;
    if l1_total_violation
        if sum(max(ci,0)) > vi_tol || norm(ce,1) > ve_tol
            return
        end
    else
        if any(ci > vi_tol) || any(abs(ce) > ve_tol)
            return
        end
    end
    tf = true;
end

function fn = getAssertFn(component,issue)
    fn = @(c,m,varargin) assertFunction(c,component,issue,m,varargin{:});
end

function assertFunction(cond,component,issue,msg,varargin)
    msg_id  = ['betaRMP:' component];
    msg_str = [issue ': ' msg];
    assert(cond,msg_id,msg_str,varargin{:});
end

function optionError(option,msg,varargin)
    msg_str = ['.' option ': ' msg];
    error('betaRMP:options',msg_str,varargin{:});
end

function tf = allFinite(x)
    tf = isnumeric(x) && ~any(~isfinite(x));
end

function tf = isVector(v)
    tf = min(size(v)) == 1;
end
